path = "bubble-128"
from boutdata import collect
from numpy import arange, mean, sqrt,roll,shape
import matplotlib.pyplot as plt
import matplotlib as mpl
import sys
import copy
sys.setrecursionlimit(2000)



mpl.rcParams.update({'font.size':14})
vort = collect("vort", path=path)
dx = collect("dx", path=path)[0,0]
dz = collect("dz", path=path)
time = collect("t_array", path=path)*1e3
nt,nx,_,nz = vort.shape
x = (arange(nx)-(nx/2.)-0.5)*dx*1e3
z = (arange(nz)-(nz/2.)+0.5)*dz*1e3

vof = collect("vof", path=path, tind=[0,100]) #1 in bubble, 0 in fluid
vof2 = copy.deepcopy(vof)

print vof[10,34,0,32]
# Function to fill all cells with VoF below threshold with value 0, those above with 1
# This is then rewritten to the vof function for further manipulation
def firstcell(timestep,i,j,vofthresh):
	global vof2
	node = vof2[timestep,i,0,j]
	#checks if node has already been painted
	if (node ==0) or (node ==1):
		return 
	#boundary conditions: exits if node is outside of grid
	#elif (i > nx-1) or (i<0):
		#return
	#elif (j > nz-1) or (j<0):
		#return
	elif node < vofthresh:
		vof2[timestep][i][0][j] = 0
	else:
		vof2[timestep][i][0][j] = 1
	

#vof indices [0-99,0-67,0,0-63] shape (101,68,1,64)


# Iterates over each grid cell and performs a cellfill on each
def firstflood(timestep,threshold):
	for i in range(0,68):
		for j in range(0,64):
			firstcell(timestep,i,j,threshold)
			
# Threshold parameter needs optimising for accurate results!

		
# Initialise a variable containing the number of bubbles
region_count = 0


def secondcell(timestep,i,j,vofthresh):
	global vof2
	global region_count
	node = vof2[timestep,i,0,j]
	if (node ==0):
		return
	
	vof2[timestep,i,0,j] = 0
	if ((i< nx-1 and i >=0) and (j< nz-1 and j >=0))==True:
		secondcell(timestep,i+1,j,vofthresh)
		secondcell(timestep,i-1,j,vofthresh)
		secondcell(timestep,i,j+1,vofthresh)
		secondcell(timestep,i,j-1,vofthresh)
	

def secondflood(timestep,threshold):
	global region_count
	for i in range(0,nx):
		for j in range(0,nz):
			if vof2[timestep,i,0,j] ==1:
				region_count +=1
				secondcell(timestep,i,j,threshold)


def floodfill(timestep,threshold):
	firstflood(timestep,threshold)
	secondflood(timestep,threshold)
	print("The number of bubbles is %s, at timestep %s with threshold %s." % (region_count,timestep,threshold))



# Flood fill testing routine; reinitialises VoF and region_count. Use floodfill unless testing VoF threshold.


for j in range(1,20):
	for i in range(0,110,10):
		floodfill(i,j*0.05)
		region_count = 0
		vof2=copy.deepcopy(vof)
