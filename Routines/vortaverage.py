import matplotlib
matplotlib.use('Agg') # prevents gui opening

path="bubble-128" #relative directory for simulation

from boutdata import collect
from numpy import arange, mean, sqrt, roll

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rcParams.update({'font.size':14})
vort = collect("vort", path=path)
dx = collect("dx", path=path)[0,0]
dz = collect("dz", path=path)

time = collect("t_array", path=path) * 1e3

nt,nx,_,nz = vort.shape

mid_x = (nx-4.)/2. + 1.5

x = (arange(nx)-(nx/2.)-0.5) * dx * 1e3
z = (arange(nz)-(nz/2.)+0.5) * dz * 1e3

#Average RMS vorticity over flat portion of graph

from input_limits import *
vrms = sqrt(mean(mean(vort**2,axis=3),axis=1))
vrms_average = mean(vrms[lower_limit:101])

#Average velocity

vof = collect("vof", path=path, tind=nt-1)
psi = collect("psi", path=path, tind=nt-1)

psi = psi[-1,:,0,:]

vx = (roll(psi, 1, axis=1) - roll(psi, -1, axis=1))/(2.*dz)
vz = -(roll(psi, 1, axis=0) - roll(psi, -1, axis=0))/(2.*dx)

vmag = sqrt(vx**2 + vz**2)
vmag_average = mean(vmag)

print "RMS vorticity averaged over flat region [kg/m3/s]", vrms_average

vrms_av_output = open("Vrms output", "w")
vrms_av_output.write("RMS vorticity = %s" % vrms_average)
vrms_av_output.close()

#Graphs average velocity (unfinished)
#plt.plot(time, vmag_average)
#plt.xlim([0,100])
#plt.xlabel("Time [ms]")
#plt.ylabel("Average velocity [m/s]")
#plt.savefig("bubble_velocity.png")
#plt.savefig("bubble_velocity.pdf")
#plt.show()
