
#!/usr/bin/env python
# 

path="bubble-128"

from boutdata import collect
from numpy import arange, mean, sqrt, roll, shape
import matplotlib.pyplot as plt
import matplotlib as mpl


mpl.rcParams.update({'font.size':14})

vort = collect("vort", path=path)
dx = collect("dx",path=path)[0,0]
dz = collect("dz", path=path)

time = collect("t_array", path=path) * 1e3

nt,nx,_,nz = vort.shape

mid_x = (nx-4.)/2. + 1.5

x =(arange(nx)-(nx/2.)-0.5) * dx * 1e3
z = (arange(nz)-(nz/2.)+0.5) * dz * 1e3

# Calculate RMS vorticity
vrms = sqrt(mean(mean(vort**2,axis=3),axis=1))



vof = collect("vof", path=path, tind=[0,100]) #vof[t,x,y,z]
psi = collect("psi", path=path, tind=[0,100])
#Time evolution of vof; this uses 1 as the first index because 0 occurs before any evolution, causes bugginess
var_png = "bubble_streamlines_-.png"
var_pdf = "bubble_streamlines_-.pdf"

for i in range(0,110,10):
	
	plt.contourf(z,x,vof[i,:,0,:],50)
#	plt.contour(z,x,psi[i,:,0,:],10)
	plt.xlabel("X [mm]")
	plt.ylabel("Z [mm]")
	
	plt.savefig(var_png.replace("-",str(i)))
#	plt.savefig(var_pdf.replace("-",str(i)))
	plt.clf()
	



