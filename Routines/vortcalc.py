
#!/usr/bin/env python
# 

path="bubble-128"

from boutdata import collect
from numpy import arange, mean, sqrt, roll, shape
import matplotlib.pyplot as plt
import matplotlib as mpl


mpl.rcParams.update({'font.size':14})

vort = collect("vort", path=path)
dx = collect("dx",path=path)[0,0]
dz = collect("dz", path=path)

time = collect("t_array", path=path) * 1e3

nt,nx,_,nz = vort.shape

mid_x = (nx-4.)/2. + 1.5

x =(arange(nx)-(nx/2.)-0.5) * dx * 1e3
z = (arange(nz)-(nz/2.)+0.5) * dz * 1e3

# Calculate RMS vorticity
vrms = sqrt(mean(mean(vort**2,axis=3),axis=1))


plt.plot(time, vrms)
plt.xlim([0,1000])
plt.xlabel("Time [ms]")
plt.ylabel(r"RMS vorticity [kg/m$^3$/s]")
plt.savefig("bubble_vorticity.pdf")
plt.savefig("bubble_vorticity.png")
plt.show()


