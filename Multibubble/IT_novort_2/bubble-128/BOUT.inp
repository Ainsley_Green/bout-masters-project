nout = 200   # Number of output steps
timestep = 1e-3 # Time between outputs

myg = 0  # No guard cells in Y since a 2D simulation

ZMAX = mesh:Lz/(2*pi) # Sets dz

[mesh]

Lx = 5e-3  # X size [m]
Lz = 5e-3  # Z size [m]

nx = nz + 4    # number of points in X, including 4 guard cells
ny = 1      # Axisymmetric
nz = 64    # Number of points in Z

xpos = x * Lx          # X position
zpos = z * Lz / (2*pi) # Z position

dx = Lx/(nx-4)

[mesh:ddx]
upwind = W3  # Upwinding scheme in X (W3 = WENO3)

[mesh:ddz]
upwind = W3  # Upwinding scheme in Z

[laplace]    # Laplacian inversion for vorticity -> stream function (psi)
type=cyclic

[solver]     # Time integration solver
maxl=10      # Maximum number of linear iterations per nonlinear step
mxstep=100000

[model]

# density of each fluid [kg/m^3]
density0 = 1000  # water = 1e3
density1 = 1.225  # air = 1.225

# Kinematic viscosity of each fluid [m^2/s]
viscosity0 = 1e-6     # water ~ 1mm^2/s (1e-6)
viscosity1 = 1e-6     # air ~ 1.48e-5

surface_tension = 7.2e-2 # 72 mN / m for water-air
curv_method = 0  # 0 = Finite Differences on smoothed VOF; 1 = Height Function method

gravity = 0.0   # Acceleration due to gravity

vof_D = 1   # Anti-diffusion in VOF advection
boussinesq = false  # Neglect variations of density in inertia?

[vorticity]

# Boundary conditions
bndry_all = neumann

# Initial condition. This produces shear flow
function = 0.0

[vof]

bndry_all=neumann

# Initial condition is 1 where there are bubbles, 0 otherwise

# Single bubble
#function = H( exp(-((x-0.5)/0.2)^2 - ((z/(2*pi) - 0.5)/0.2)^2 ) - 0.5 )

# Single bubble, smoothed edges
#function = 0.5*tanh( (exp(-((x-0.5)/0.2)^2 - ((z/(2*pi) - 0.5)/0.2)^2 ) - 0.5)/0.01 )+0.5

# Square bubble
#function = H(exp(-((x-0.5)/0.2)^2) - 0.5) * H(exp(-((z/(2*pi) - 0.5)/0.2)^2 ) - 0.5)

# Alternating layers of fluid with some noise in the boundary
#function = H( exp(-((x-0.5)/0.2)^2 ) + 0.01*mixmode(2.*z)- 0.5 )

# A drop above a flat surface
#function = H( 0.5 - exp(-((x-0.5)/0.1)^2 - ((z/(2*pi) - 0.5)/0.1)^2 ) - H(0.2 - x))

# Initial multiple bubble simulation, with 3 bubbles evenly spaced in x
bubble1=0.5*tanh((exp(-((x-0.1)/0.05)^2 - ((z/(2*pi)-0.5)/0.05)^2)-0.5)/0.01)+0.5
bubble2=0.5*tanh((exp(-((x-0.7)/0.2)^2 - ((z/(2*pi)-0.5)/0.2)^2)-0.5)/0.01)+0.5
bubble3=0.5*tanh((exp(-((x-0.9)/0.05)^2 - ((z/(2*pi)-0.5)/0.05)^2)-0.5)/0.01)+0.5
function = bubble1  + bubble3
